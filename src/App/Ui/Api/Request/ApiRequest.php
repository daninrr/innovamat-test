<?php


namespace App\Ui\Api\Request;

use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class ApiRequest implements \App\Ui\Api\Http\ApiRequest
{
    public array $errors = [];

    public function isValid(): bool
    {
        return empty($this->errors);
    }

    public function setErrors(ConstraintViolationListInterface $errors): void
    {
        // Remove array keys
        $re = '/(\[\d*\])/';

        foreach ($errors as $error) {
            $this->errors[preg_replace($re, '', $error->getPropertyPath())] = $error->getMessage();
        }
    }

    public function addError(string $propertyName, string $error): void
    {
        $this->errors[$propertyName] = $error;
    }

    public function error(string $propertyName): ?string
    {
        return $this->errors[$propertyName] ?? null;
    }

    public function validationGroups(): array
    {
        return [];
    }
}
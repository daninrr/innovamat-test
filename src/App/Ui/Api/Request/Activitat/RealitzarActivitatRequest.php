<?php

namespace App\Ui\Api\Request\Activitat;

use App\Ui\Api\Request\ApiRequest;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class RealitzarActivitatRequest extends ApiRequest
{
    private const ACTIVITAT_ID = 'activitat_id';
    private const SOLUCIO = 'solucio';
    private const TEMPS = 'temps';

    public ?string $activitatId;
    public ?string $solucio;
    public ?int $temps;

    public function __construct(
        ?string $activitatId = null,
        ?string $solucio = null,
        ?int $temps = null
    ) {
        $this->activitatId = $activitatId;
        $this->solucio = $solucio;
        $this->temps = $temps;
    }

    public static function fromRequestArrayData(array $data): \App\Ui\Api\Http\ApiRequest
    {
        return new self(
            $data[self::ACTIVITAT_ID] ?? null,
            $data[self::SOLUCIO] ?? null,
            $data[self::TEMPS] ?? null,
        );
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('activitatId', new Assert\NotBlank());
        $metadata->addPropertyConstraint('activitatId', new Assert\NotNull());

        $metadata->addPropertyConstraint('solucio', new Assert\NotBlank());
        $metadata->addPropertyConstraint('solucio', new Assert\NotNull());

        $metadata->addPropertyConstraint('temps', new Assert\NotBlank());
        $metadata->addPropertyConstraint('temps', new Assert\NotNull());
    }
}
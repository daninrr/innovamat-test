<?php

namespace App\Ui\Api\Request\Activitat;

use App\Ui\Api\Request\ApiRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class AfegirActivitatRequest extends ApiRequest
{
    private const NOM = 'nom';
    private const SOLUCIO = 'solucio';
    private const NIVELL = 'nivell';
    private const TEMPS_TOTAL_ESTIMAT = 'temps_total_estimat';

    public ?string $nom;
    public ?string $solucio;
    public ?int $nivell;
    public ?int $tempsTotalEstimat;

    public function __construct(
        ?string $nom = null,
        ?string $solucio = null,
        ?int $nivell = null,
        ?int $tempsTotalEstimat = null
    ) {
        $this->nom = $nom;
        $this->solucio = $solucio;
        $this->nivell = $nivell;
        $this->tempsTotalEstimat = $tempsTotalEstimat;
    }

    public static function fromRequestArrayData(array $data): \App\Ui\Api\Http\ApiRequest
    {
        return new self(
            $data[self::NOM] ?? null,
            $data[self::SOLUCIO] ?? null,
            $data[self::NIVELL] ?? null,
            $data[self::TEMPS_TOTAL_ESTIMAT] ?? null,
        );
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('nom', new Assert\NotBlank());
        $metadata->addPropertyConstraint('nom', new Assert\NotNull());

        $metadata->addPropertyConstraint('solucio', new Assert\NotBlank());
        $metadata->addPropertyConstraint('solucio', new Assert\NotNull());

        $metadata->addPropertyConstraint('nivell', new Assert\NotBlank());
        $metadata->addPropertyConstraint('nivell', new Assert\NotNull());

        $metadata->addPropertyConstraint('tempsTotalEstimat', new Assert\NotBlank());
        $metadata->addPropertyConstraint('tempsTotalEstimat', new Assert\NotNull());
    }
}
<?php

namespace App\Ui\Api\Controller\Activitat;

use App\Application\Command\Activitat\RealitzarActivitatCommand;
use App\Application\Command\Activitat\RealitzarActivitatCommandHandler;
use App\Domain\Exception\Service\Activitat\RespostaEstudiantIsNotValid;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Infrastructure\Api\Problem\Activitat\RealitzarActivitatProblem;
use App\Ui\Api\Request\Activitat\RealitzarActivitatRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

class RealitzarActivitatController extends AbstractController
{
    /**
     * @return JsonResponse
     *
     * @SWG\Response(
     *     response="200",
     *     description="Guarda la resposta i puntuació del estudiant",
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="name", type="string"),
     *          example={
     *              "activitat_id": "ff1764ec-a966-43b8-b8ef-410f1d8be00b",
     *              "solucio": "1_0_2",
     *              "temps": "90"
     *          }
     *     )
     * )
     * @SWG\Tag(name="Activitat")
     */
    public function __invoke(
        RealitzarActivitatRequest $request,
        RealitzarActivitatCommandHandler $realitzarActivitatCommandHandler
    ): JsonResponse {

        if (false === $request->isValid()) {
            return $this->requestIsNotValidError($request->errors);
        }

        try {
            $puntuacio = $realitzarActivitatCommandHandler->__invoke(
                new RealitzarActivitatCommand(
                    $request->activitatId,
                    $request->solucio,
                    $request->temps
                )
            );
        } catch (RespostaEstudiantIsNotValid | SeguentActivitatNotFound $exception) {
            return $this->serveErrorWithExceptionResponse($exception);
        }

        return $this->validResponse($puntuacio);
    }

    private function validResponse(int $puntuacio): JsonResponse
    {
        return new JsonResponse(
            [
                'puntuacio' => $puntuacio
            ],
            Response::HTTP_OK
        );
    }

    private function requestIsNotValidError(array $errors): JsonResponse
    {
        return $this->serveErrorResponse(
            RealitzarActivitatProblem::becauseSentDataIsErroneous($errors)
        );
    }

    private function serveErrorResponse(RealitzarActivitatProblem $problem): JsonResponse
    {
        return JsonResponse::fromJsonString(
            $problem->asJson(),
            Response::HTTP_BAD_REQUEST
        );
    }

    private function serveErrorWithExceptionResponse(\Exception $exception): JsonResponse
    {
        return JsonResponse::fromJsonString(
            RealitzarActivitatProblem::withException($exception)->asJson(),
            Response::HTTP_BAD_REQUEST
        );
    }
}
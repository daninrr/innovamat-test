<?php

namespace App\Ui\Api\Controller\Activitat;

use App\Application\Query\Activitat\SeguentActivitatQuery;
use App\Application\Query\Activitat\SeguentActivitatQueryHandler;
use App\Domain\Dto\Activitat\ActivitatBasicDto;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Infrastructure\Api\Problem\Activitat\SeguentActivitatProblem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

class SeguentActivitatController extends AbstractController
{
    /**
     * @return JsonResponse
     *
     * @SWG\Response(
     *     response="200",
     *     description="Retorna la següent activitat a realitzar",
     * )
     *
     * @SWG\Tag(name="Activitat")
     */
    public function __invoke(SeguentActivitatQueryHandler $seguentActivitatQueryHandler): JsonResponse
    {
        try {
            $properaActivitat = $seguentActivitatQueryHandler->__invoke(
                new SeguentActivitatQuery()
            );
        } catch (SeguentActivitatNotFound $exception) {
            return $this->serveErrorWithExceptionResponse($exception);
        }

        return $this->validResponse($properaActivitat);
    }

    private function validResponse(ActivitatBasicDto $properaActivitat): JsonResponse
    {
        return new JsonResponse(
            $properaActivitat,
            Response::HTTP_OK
        );
    }

    private function serveErrorWithExceptionResponse(\Exception $exception): JsonResponse
    {
        return JsonResponse::fromJsonString(
            SeguentActivitatProblem::withException($exception)->asJson(),
            Response::HTTP_BAD_REQUEST
        );
    }
}
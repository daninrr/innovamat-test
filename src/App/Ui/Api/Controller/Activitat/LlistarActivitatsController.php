<?php

namespace App\Ui\Api\Controller\Activitat;

use App\Application\Query\Activitat\LlistarActivitatsQuery;
use App\Application\Query\Activitat\LlistarActivitatsQueryHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

class LlistarActivitatsController extends AbstractController
{
    /**
     * @return JsonResponse
     *
     * @SWG\Response(
     *     response="200",
     *     description="Retorna un llistat de totes les activitats",
     * )
     *
     * @SWG\Tag(name="Activitat")
     */
    public function __invoke(LlistarActivitatsQueryHandler $llistarActivitatsQueryHandler): JsonResponse
    {
        $activitats = $llistarActivitatsQueryHandler->__invoke(
            new LlistarActivitatsQuery()
        );

        return $this->validResponse($activitats);
    }

    private function validResponse(array $activitats): JsonResponse
    {
        $response = new JsonResponse(
            $activitats,
            Response::HTTP_OK
        );

        return $response;
    }
}
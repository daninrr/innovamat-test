<?php

namespace App\Ui\Api\Controller\Activitat;

use App\Application\Command\Activitat\AfegirActivitatCommand;
use App\Application\Command\Activitat\AfegirActivitatCommandHandler;
use App\Infrastructure\Api\Problem\Activitat\AfegirActivitatProblem;
use App\Ui\Api\Request\Activitat\AfegirActivitatRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AfegirActivitatController extends AbstractController
{
    /**
     * @param AfegirActivitatRequest $request
     *
     * @return JsonResponse
     *
     * @SWG\Response(
     *     response="200",
     *     description="Crea una nova activitat",
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="name", type="string"),
     *          example={
     *              "nom": "A1",
     *              "nivell": "1",
     *              "temps_total_estimat": "120",
     *              "solucio": "1_0_2"
     *          }
     *     )
     * )
     * @SWG\Tag(name="Activitat")
     */
    public function __invoke(AfegirActivitatRequest $request, AfegirActivitatCommandHandler $afegirActivitatCommandHandler): JsonResponse
    {
        if (false === $request->isValid()) {
            return $this->requestIsNotValidError($request->errors);
        }

        $afegirActivitatCommandHandler->__invoke(
            new AfegirActivitatCommand(
                $request->nom,
                $request->solucio,
                $request->nivell,
                $request->tempsTotalEstimat
            )
        );

        return $this->validResponse();
    }

    private function validResponse(): JsonResponse
    {
        $response = new JsonResponse(
            [],
            Response::HTTP_OK
        );

        return $response;
    }

    private function requestIsNotValidError(array $errors): JsonResponse
    {
        return $this->serveErrorResponse(
            AfegirActivitatProblem::becauseSentDataIsErroneous($errors)
        );
    }

    private function serveErrorResponse(AfegirActivitatProblem $problem): JsonResponse
    {
        return JsonResponse::fromJsonString(
            $problem->asJson(),
            Response::HTTP_BAD_REQUEST
        );
    }
}
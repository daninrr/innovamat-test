<?php

namespace App\Ui\Api\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ApiRequestResolver implements ArgumentValueResolverInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        try {
            $reflection = new \ReflectionClass($argument->getType());
            if ($reflection->implementsInterface(ApiRequest::class)) {
                return true;
            }
        } catch (\ReflectionException $e) {
        }

        return false;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $class = $argument->getType();

        /** @var ApiRequest $apiRequest */
        $apiRequest = $class::fromRequestArrayData(iterator_to_array($request->request));

        // throw bad request exception in case of invalid request data
        $errors = $this->validator->validate($apiRequest, null, $apiRequest->validationGroups());
        if (\count($errors) > 0) {
            $apiRequest->setErrors($errors);
        }

        yield $apiRequest;
    }
}
<?php

namespace App\Ui\Api\Http;

use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ApiRequest
{
    public static function fromRequestArrayData(array $data): ApiRequest;

    public function isValid(): bool;

    public function setErrors(ConstraintViolationListInterface $errors): void;

    public function addError(string $propertyName, string $error): void;

    public function error(string $propertyName): ?string;

    public function validationGroups(): array;
}
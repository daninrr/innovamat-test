<?php

namespace App\Domain\Exception\Model\Nivell;

class NivellNotFound extends \Exception
{
    public static function becauseNumeroDoesNotExist(int $numero): self
    {
        return new self(
            sprintf(
                'No hi ha nivell %s en aquest itinerari',
                $numero
            )
        );
    }
}
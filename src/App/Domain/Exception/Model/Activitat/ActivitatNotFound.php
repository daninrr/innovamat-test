<?php

namespace App\Domain\Exception\Model\Activitat;

use App\Domain\ValueObject\Id;

class ActivitatNotFound extends \Exception
{
    public static function becauseIdDoesNotExist(Id $id): self
    {
        return new self(
            sprintf(
                'No hi ha cap activitat amb id %s',
                $id->asString()
            )
        );
    }
}
<?php

namespace App\Domain\Exception\Service\Activitat;

class SeguentActivitatNotFound extends \Exception
{
    public static function becauseNoMoreActivitats(): self
    {
        return new self(
            sprintf(
                'No queden més activitats en aquest itinerari',
            )
        );
    }
}
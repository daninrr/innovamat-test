<?php

namespace App\Domain\Exception\Service\Activitat;

class AnteriorActivitatNotFound extends \Exception
{
    public static function becauseIsFirstActivitat(): self
    {
        return new self(
            sprintf(
                'Aquesta es la primera activitat del itinerari',
            )
        );
    }
}
<?php

namespace App\Domain\Exception\Service\Activitat;

class RespostaEstudiantIsNotValid extends \Exception
{
    public static function becauseSeguentActivitatDoesNotMatch(): self
    {
        return new self(
            sprintf(
                'No et toca fer aquesta activitat de l\'itinerari',
            )
        );
    }

    public static function becauseNumeroSolucionsDoesNotMatch(): self
    {
        return new self(
            sprintf(
                'No coincideixen el número de solucions enviades amb els exercicis de l\'activitat',
            )
        );
    }
}
<?php

namespace App\Domain\Model\Activitat;

use App\Domain\Model\Activitat;
use App\Domain\ValueObject\Id;

class RespostaEstudiant
{
    private Id $id;
    private string $solucio;
    private int $temps;
    private int $puntuacio;
    private \DateTime $creadaEn;
    private Activitat $activitat;

    private function __construct(Id $id, string $solucio, int $temps, int $puntuacio, Activitat $activitat)
    {
        $this->id = $id;
        $this->solucio = $solucio;
        $this->temps = $temps;
        $this->puntuacio = $puntuacio;
        $this->activitat = $activitat;

        $this->creadaEn = new \DateTime();
    }

    public static function create(Id $id, string $solucio, int $temps, int $puntuacio, Activitat $activitat): self
    {
        return new self(
            $id,
            $solucio,
            $temps,
            $puntuacio,
            $activitat
        );
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getSolucio(): string
    {
        return $this->solucio;
    }

    public function getTemps(): int
    {
        return $this->temps;
    }

    public function getPuntuacio(): int
    {
        return $this->puntuacio;
    }

    public function getCreadaEn(): \DateTime
    {
        return $this->creadaEn;
    }

    public function getActivitat(): Activitat
    {
        return $this->activitat;
    }

    public function getActivitatNivellNumero(): int
    {
        return $this->activitat->getNivellNumero();
    }

    public function getActivitatOrdre(): int
    {
        return $this->activitat->getOrdre();
    }

    public function getActivitatTotalTempsEstimat(): int
    {
        return $this->activitat->getTempsTotalEstimat();
    }
}
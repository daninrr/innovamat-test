<?php

namespace App\Domain\Model;

use App\Domain\ValueObject\Id;

class Activitat
{
    private Id $id;
    private string $nom;
    private string $solucio;
    private int $ordre;
    private int $tempsTotalEstimat;
    private Nivell $nivell;

    public function __construct(
        Id $id,
        string $nom,
        string $solucio,
        int $ordre,
        int $tempsTotalEstimat,
        Nivell $nivell
    ) {
        $this->id = $id;
        $this->nom = $nom;
        $this->solucio = $solucio;
        $this->ordre = $ordre;
        $this->tempsTotalEstimat = $tempsTotalEstimat;
        $this->nivell = $nivell;
    }

    public static function create(
        Id $id,
        string $nom,
        string $solucio,
        int $ordre,
        int $tempsTotalEstimat,
        Nivell $nivell
    ): self {
        return new self(
            $id,
            $nom,
            $solucio,
            $ordre,
            $tempsTotalEstimat,
            $nivell
        );
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getSolucio(): string
    {
        return $this->solucio;
    }

    public function getOrdre(): int
    {
        return $this->ordre;
    }

    public function getTempsTotalEstimat(): int
    {
        return $this->tempsTotalEstimat;
    }

    public function getNivell(): Nivell
    {
        return $this->nivell;
    }

    public function getNivellNumero(): int
    {
        return $this->nivell->getNumero();
    }
}
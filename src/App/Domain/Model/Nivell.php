<?php

namespace App\Domain\Model;

use App\Domain\ValueObject\Id;

class Nivell
{
    private Id $id;
    private int $numero;

    public function __construct(Id $id, int $numero)
    {
        $this->id = $id;
        $this->numero = $numero;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getNumero(): int
    {
        return $this->numero;
    }
}
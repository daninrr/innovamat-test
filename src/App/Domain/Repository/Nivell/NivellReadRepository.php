<?php

namespace App\Domain\Repository\Nivell;

use App\Domain\Exception\Model\Nivell\NivellNotFound;
use App\Domain\Model\Nivell;

interface NivellReadRepository
{
    /**
     * @throws NivellNotFound
     */
    public function ofNumeroOrFail(int $numero): Nivell;
}
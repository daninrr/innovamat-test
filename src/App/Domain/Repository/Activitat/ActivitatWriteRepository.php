<?php

namespace App\Domain\Repository\Activitat;

use App\Domain\Model\Activitat;
use App\Domain\ValueObject\Id;

interface ActivitatWriteRepository
{
    public function save(Activitat $activitat): void;
    public function nextIdentity(): Id;
}
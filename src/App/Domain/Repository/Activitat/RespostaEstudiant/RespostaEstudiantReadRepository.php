<?php

namespace App\Domain\Repository\Activitat\RespostaEstudiant;

use App\Domain\Model\Activitat\RespostaEstudiant;
use App\Domain\ValueObject\Id;

interface RespostaEstudiantReadRepository
{
    public function last(): ?RespostaEstudiant;
    public function previousToLast(Id $lastRespostaId): ?RespostaEstudiant;
}
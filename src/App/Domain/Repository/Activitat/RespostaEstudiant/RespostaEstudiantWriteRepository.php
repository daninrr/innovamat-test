<?php

namespace App\Domain\Repository\Activitat\RespostaEstudiant;

use App\Domain\Model\Activitat\RespostaEstudiant;
use App\Domain\ValueObject\Id;

interface RespostaEstudiantWriteRepository
{
    public function save(RespostaEstudiant $respostaEstudiant): void;
    public function nextIdentity(): Id;
}
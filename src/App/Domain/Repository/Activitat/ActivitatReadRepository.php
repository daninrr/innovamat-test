<?php

namespace App\Domain\Repository\Activitat;

use App\Domain\Dto\Activitat\ActivitatBasicDto;
use App\Domain\Exception\Model\Activitat\ActivitatNotFound;
use App\Domain\Exception\Service\Activitat\AnteriorActivitatNotFound;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Domain\Model\Activitat;
use App\Domain\ValueObject\Id;

interface ActivitatReadRepository
{
    public function all(): array;
    public function first(): ActivitatBasicDto;
    public function count(): int;

    /**
     * @throws SeguentActivitatNotFound
     */
    public function seguentActivitatPerOrdre(int $ordreActual): ActivitatBasicDto;

    /**
     * @throws AnteriorActivitatNotFound
     */
    public function anteriorActivitatPerOrdre(int $ordreActual): ActivitatBasicDto;

    /**
     * @throws SeguentActivitatNotFound
     */
    public function primeraActivitatSeguentNivell(int $nivellActual): ActivitatBasicDto;

    /**
     * @throws ActivitatNotFound
     */
    public function ofIdOrFail(Id $id): Activitat;
}
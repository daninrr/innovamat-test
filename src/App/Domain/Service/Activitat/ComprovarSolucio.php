<?php

namespace App\Domain\Service\Activitat;

use App\Domain\Exception\Model\Activitat\ActivitatNotFound;
use App\Domain\Exception\Service\Activitat\RespostaEstudiantIsNotValid;
use App\Domain\Repository\Activitat\ActivitatReadRepository;
use App\Domain\ValueObject\Id;

class ComprovarSolucio
{
    private ActivitatReadRepository $activitatReadRepository;

    public function __construct(ActivitatReadRepository $activitatReadRepository)
    {
        $this->activitatReadRepository = $activitatReadRepository;
    }

    /**
     * @throws RespostaEstudiantIsNotValid
     * @throws ActivitatNotFound
     */
    public function __invoke(Id $activitatId, string $solucio): int
    {
        $activitat = $this->activitatReadRepository->ofIdOrFail($activitatId);

        if ($activitat->getSolucio() === $solucio) {
            return 100;
        }

        $solucionsActivitat = explode('_', $activitat->getSolucio());
        $solucionsEstudiant = explode('_', $solucio);

        if (count($solucionsActivitat) !== count($solucionsEstudiant)) {
            throw RespostaEstudiantIsNotValid::becauseNumeroSolucionsDoesNotMatch();
        }

        $countExercicis = \count($solucionsActivitat);
        $countSolucionsCorrectes = 0;

        foreach ($solucionsEstudiant as $key => $item) {
            if ($item === $solucionsActivitat[$key]) {
                $countSolucionsCorrectes++;
            }
        }

        return round(($countSolucionsCorrectes / $countExercicis) * 100);
    }
}
<?php

namespace App\Domain\Service\Activitat;

use App\Domain\Dto\Activitat\ActivitatBasicDto;
use App\Domain\Exception\Service\Activitat\AnteriorActivitatNotFound;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Domain\Repository\Activitat\ActivitatReadRepository;
use App\Domain\Repository\Activitat\RespostaEstudiant\RespostaEstudiantReadRepository;

class ComprovarSeguentActivitat
{
    private ActivitatReadRepository $activitatReadRepository;
    private RespostaEstudiantReadRepository $respostaEstudiantReadRepository;

    public function __construct(
        ActivitatReadRepository $activitatReadRepository,
        RespostaEstudiantReadRepository $respostaEstudiantReadRepository
    ) {
        $this->activitatReadRepository = $activitatReadRepository;
        $this->respostaEstudiantReadRepository = $respostaEstudiantReadRepository;
    }

    /**
     * @throws SeguentActivitatNotFound
     * @throws AnteriorActivitatNotFound
     */
    public function __invoke(): ActivitatBasicDto
    {
        $lastRespostaEstudiant = $this->respostaEstudiantReadRepository->last();

        // Si encara no té respostes, es retorna la primera activitat de l'itinerari.
        if (null === $lastRespostaEstudiant) {
            return $this->activitatReadRepository->first();
        }

        $respostaTempsPercentage = $this->calculateTempsPercentage(
            $lastRespostaEstudiant->getTemps(),
            $lastRespostaEstudiant->getActivitatTotalTempsEstimat()
        );

        if ($lastRespostaEstudiant->getPuntuacio() > 75 && $respostaTempsPercentage < 50) {
            // Si score > 75% & temps < 50% del temps estimat llavors s'avança un nivell
            return $this->activitatReadRepository->primeraActivitatSeguentNivell($lastRespostaEstudiant->getActivitatNivellNumero());
        } elseif ($lastRespostaEstudiant->getPuntuacio() < 20) {
            // Resposta prèvia a la última per saber si hi ha salt de nivell o no.
            $previousToLastRespostaEstudiant = $this->respostaEstudiantReadRepository->previousToLast($lastRespostaEstudiant->getId());

            if (null !== $previousToLastRespostaEstudiant &&
                abs($previousToLastRespostaEstudiant->getActivitatOrdre() - $lastRespostaEstudiant->getActivitatOrdre()) !== 1)
            {
                return $this->activitatReadRepository->seguentActivitatPerOrdre($previousToLastRespostaEstudiant->getActivitatOrdre());
            }
        }

        // Si la puntuació de l'activitat es menor d'un 75% es retorna la mateixa activitat.
        if ($lastRespostaEstudiant->getPuntuacio() < 75) {
            return ActivitatBasicDto::createFromModel($lastRespostaEstudiant->getActivitat());
        }

        // Si ha superat un 75% de puntuació llavors es retorna la següent activitat.
        return $this->activitatReadRepository->seguentActivitatPerOrdre($lastRespostaEstudiant->getActivitatOrdre());
    }

    private function calculateTempsPercentage(int $tempsResposta, int $tempsActivitatEstimat): int
    {
        return round(($tempsResposta / $tempsActivitatEstimat) * 100);
    }
}
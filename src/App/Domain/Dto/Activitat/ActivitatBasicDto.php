<?php

namespace App\Domain\Dto\Activitat;

use App\Domain\Model\Activitat;
use App\Domain\ValueObject\Id;

class ActivitatBasicDto implements \JsonSerializable
{
    public Id $id;
    public string $nom;
    public int $numeroNivell;
    public int $tempsTotalEstimat;

    public function __construct(Id $id, string $nom, int $numeroNivell, int $tempsTotalEstimat)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->numeroNivell = $numeroNivell;
        $this->tempsTotalEstimat = $tempsTotalEstimat;
    }

    public static function createFromModel(Activitat $activitat): self
    {
        return new self(
            $activitat->getId(),
            $activitat->getNom(),
            $activitat->getNivellNumero(),
            $activitat->getTempsTotalEstimat()
        );
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id->asString(),
            'nom' => $this->nom,
            'numero_nivell' => $this->numeroNivell,
            'temps_total_estimat' => $this->tempsTotalEstimat,
        ];
    }
}
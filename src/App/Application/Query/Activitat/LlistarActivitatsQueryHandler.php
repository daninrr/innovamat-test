<?php

namespace App\Application\Query\Activitat;

use App\Domain\Repository\Activitat\ActivitatReadRepository;

class LlistarActivitatsQueryHandler
{
    private ActivitatReadRepository $activitatReadRepository;

    public function __construct(ActivitatReadRepository $activitatReadRepository)
    {
        $this->activitatReadRepository = $activitatReadRepository;
    }

    public function __invoke(LlistarActivitatsQuery $query): array
    {
        return $this->activitatReadRepository->all();
    }
}
<?php

namespace App\Application\Query\Activitat;

use App\Domain\Dto\Activitat\ActivitatBasicDto;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Domain\Service\Activitat\ComprovarSeguentActivitat;

class SeguentActivitatQueryHandler
{
    private ComprovarSeguentActivitat $comprovarSeguentActivitatService;

    public function __construct(ComprovarSeguentActivitat $comprovarSeguentActivitatService)
    {
        $this->comprovarSeguentActivitatService = $comprovarSeguentActivitatService;
    }

    /**
     * @throws SeguentActivitatNotFound
     */
    public function __invoke(SeguentActivitatQuery $query): ActivitatBasicDto
    {
        return $this->comprovarSeguentActivitatService->__invoke();
    }
}
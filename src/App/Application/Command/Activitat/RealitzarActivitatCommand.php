<?php

namespace App\Application\Command\Activitat;

class RealitzarActivitatCommand
{
    public string $activitatId;
    public string $solucio;
    public int $temps;

    public function __construct(string $activitatId, string $solucio, int $temps)
    {
        $this->activitatId = $activitatId;
        $this->solucio = $solucio;
        $this->temps = $temps;
    }
}
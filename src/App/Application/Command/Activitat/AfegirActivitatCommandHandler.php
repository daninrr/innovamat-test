<?php

namespace App\Application\Command\Activitat;

use App\Domain\Exception\Model\Nivell\NivellNotFound;
use App\Domain\Model\Activitat;
use App\Domain\Repository\Activitat\ActivitatReadRepository;
use App\Domain\Repository\Activitat\ActivitatWriteRepository;
use App\Domain\Repository\Nivell\NivellReadRepository;

class AfegirActivitatCommandHandler
{
    private ActivitatReadRepository $activitatReadRepository;
    private ActivitatWriteRepository $activitatWriteRepository;
    private NivellReadRepository $nivellReadRepository;

    public function __construct(
        ActivitatReadRepository $activitatReadRepository,
        ActivitatWriteRepository $activitatWriteRepository,
        NivellReadRepository $nivellReadRepository
    ) {
        $this->activitatReadRepository = $activitatReadRepository;
        $this->activitatWriteRepository = $activitatWriteRepository;
        $this->nivellReadRepository = $nivellReadRepository;
    }

    /**
     * @throws NivellNotFound
     */
    public function __invoke(AfegirActivitatCommand $command): void
    {
        $numeroActivitats = $this->activitatReadRepository->count();

        $activitat = Activitat::create(
            $this->activitatWriteRepository->nextIdentity(),
            $command->nom,
            $command->solucio,
            $numeroActivitats + 1,
            $command->tempsTotalEstimat,
            $this->nivellReadRepository->ofNumeroOrFail(
                $command->nivell
            )
        );

        $this->activitatWriteRepository->save($activitat);
    }
}
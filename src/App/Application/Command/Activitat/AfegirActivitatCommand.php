<?php

namespace App\Application\Command\Activitat;

class AfegirActivitatCommand
{
    public string $nom;
    public string $solucio;
    public int $nivell;
    public int $tempsTotalEstimat;

    public function __construct(string $nom, string $solucio, int $nivell, int $tempsTotalEstimat)
    {
        $this->nom = $nom;
        $this->solucio = $solucio;
        $this->nivell = $nivell;
        $this->tempsTotalEstimat = $tempsTotalEstimat;
    }
}
<?php

namespace App\Application\Command\Activitat;

use App\Domain\Exception\Model\Activitat\ActivitatNotFound;
use App\Domain\Exception\Service\Activitat\RespostaEstudiantIsNotValid;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Domain\Model\Activitat\RespostaEstudiant;
use App\Domain\Repository\Activitat\ActivitatReadRepository;
use App\Domain\Repository\Activitat\RespostaEstudiant\RespostaEstudiantWriteRepository;
use App\Domain\Service\Activitat\ComprovarSeguentActivitat;
use App\Domain\Service\Activitat\ComprovarSolucio;
use App\Domain\ValueObject\Id;

class RealitzarActivitatCommandHandler
{
    private RespostaEstudiantWriteRepository $respostaEstudiantWriteRepository;
    private ActivitatReadRepository $activitatReadRepository;
    private ComprovarSeguentActivitat $comprovarSeguentActivitatService;
    private ComprovarSolucio $comprovarSolucioService;

    public function __construct(
        RespostaEstudiantWriteRepository $respostaEstudiantWriteRepository,
        ActivitatReadRepository $activitatReadRepository,
        ComprovarSeguentActivitat $comprovarSeguentActivitatService,
        ComprovarSolucio $comprovarSolucioService
    ) {
        $this->respostaEstudiantWriteRepository = $respostaEstudiantWriteRepository;
        $this->activitatReadRepository = $activitatReadRepository;
        $this->comprovarSeguentActivitatService = $comprovarSeguentActivitatService;
        $this->comprovarSolucioService = $comprovarSolucioService;
    }

    /**
     * @throws ActivitatNotFound
     * @throws RespostaEstudiantIsNotValid
     * @throws SeguentActivitatNotFound
     */
    public function __invoke(RealitzarActivitatCommand $command): int
    {
        $activitatId = Id::fromString($command->activitatId);
        $seguentActivitat = $this->comprovarSeguentActivitatService->__invoke();

        if (!$activitatId->equalsTo($seguentActivitat->id)) {
            throw RespostaEstudiantIsNotValid::becauseSeguentActivitatDoesNotMatch();
        }

        $puntuacio = $this->comprovarSolucioService->__invoke(
            $activitatId,
            $command->solucio
        );

        $respostaEstudiant = RespostaEstudiant::create(
            $this->respostaEstudiantWriteRepository->nextIdentity(),
            $command->solucio,
            $command->temps,
            $puntuacio,
            $this->activitatReadRepository->ofIdOrFail($activitatId)
        );

        $this->respostaEstudiantWriteRepository->save($respostaEstudiant);

        return $puntuacio;
    }
}
<?php

namespace App\Infrastructure\Persistence\Doctrine\Repository\Activitat;

use App\Domain\Model\Activitat;
use App\Domain\ValueObject\Id;
use Doctrine\ORM\EntityManagerInterface;

class ActivitatWriteRepository implements \App\Domain\Repository\Activitat\ActivitatWriteRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(Activitat $activitat): void
    {
        $this->entityManager->persist($activitat);
        $this->entityManager->flush();
    }

    public function nextIdentity(): Id
    {
        return Id::generate();
    }
}
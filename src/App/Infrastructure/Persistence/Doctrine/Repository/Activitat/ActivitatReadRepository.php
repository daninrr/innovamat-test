<?php

namespace App\Infrastructure\Persistence\Doctrine\Repository\Activitat;

use App\Domain\Dto\Activitat\ActivitatBasicDto;
use App\Domain\Exception\Model\Activitat\ActivitatNotFound;
use App\Domain\Exception\Service\Activitat\AnteriorActivitatNotFound;
use App\Domain\Exception\Service\Activitat\SeguentActivitatNotFound;
use App\Domain\Model\Activitat;
use App\Domain\ValueObject\Id;
use App\Infrastructure\Persistence\Doctrine\ValueObject\IdType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class ActivitatReadRepository implements \App\Domain\Repository\Activitat\ActivitatReadRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function all(): array
    {
        $sql = <<<EOF
SELECT activitat.id,
activitat.nom,
nivell.numero as nivell,
activitat.temps_total_estimat
FROM activitat
INNER JOIN nivell ON nivell.id = activitat.nivell_id
ORDER BY activitat.ordre ASC
EOF;
        $stmt = $this
            ->entityManager
            ->getConnection()
            ->prepare(
                $sql
            );

        $stmt->execute();

        return $stmt->fetchAllAssociative();
    }

    /**
     * @throws SeguentActivitatNotFound
     */
    public function seguentActivitatPerOrdre(int $ordreActual): ActivitatBasicDto
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('activitat')
            ->from(Activitat::class, 'activitat')

            ->where('activitat.ordre > :ordre_actual')
            ->setParameter('ordre_actual', $ordreActual)

            ->orderBy('activitat.ordre', 'ASC')
            ->setMaxResults(1)
            ->getFirstResult()
        ;

        try {
            $activitat = $qb
                ->getQuery()
                ->getSingleResult()
            ;
        } catch (NoResultException | NonUniqueResultException $e) {
            throw SeguentActivitatNotFound::becauseNoMoreActivitats();
        }

        return ActivitatBasicDto::createFromModel($activitat);
    }

    /**
     * @throws AnteriorActivitatNotFound
     */
    public function anteriorActivitatPerOrdre(int $ordreActual): ActivitatBasicDto
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('activitat')
            ->from(Activitat::class, 'activitat')

            ->where('activitat.ordre < :ordre_actual')
            ->setParameter('ordre_actual', $ordreActual)

            ->orderBy('activitat.ordre', 'ASC')
            ->setMaxResults(1)
            ->getFirstResult()
        ;

        try {
            $activitat = $qb
                ->getQuery()
                ->getSingleResult()
            ;
        } catch (NoResultException | NonUniqueResultException $e) {
            throw AnteriorActivitatNotFound::becauseIsFirstActivitat();
        }

        return ActivitatBasicDto::createFromModel($activitat);
    }

    public function primeraActivitatSeguentNivell(int $nivellActual): ActivitatBasicDto
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('activitat')
            ->from(Activitat::class, 'activitat')
            ->innerJoin('activitat.nivell', 'nivell')

            ->where('nivell.numero > :nivell_actual')
            ->setParameter('nivell_actual', $nivellActual)

            ->orderBy('activitat.ordre', 'ASC')
            ->setMaxResults(1)
            ->getFirstResult()
        ;

        try {
            $activitat = $qb
                ->getQuery()
                ->getSingleResult()
            ;
        } catch (NoResultException | NonUniqueResultException $e) {
            throw SeguentActivitatNotFound::becauseNoMoreActivitats();
        }

        return ActivitatBasicDto::createFromModel($activitat);
    }

    public function first(): ActivitatBasicDto
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('activitat')
            ->from(Activitat::class, 'activitat')
            ->orderBy('activitat.ordre', 'ASC')
            ->setMaxResults(1)
            ->getFirstResult()
        ;

        $activitat = $qb
            ->getQuery()
            ->getSingleResult()
        ;

        return ActivitatBasicDto::createFromModel($activitat);
    }

    public function count(): int
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('COUNT(activitat)')
            ->from(Activitat::class, 'activitat')
        ;

        return $qb
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }


    /**
     * @throws ActivitatNotFound
     */
    public function ofIdOrFail(Id $id): Activitat
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('activitat')
            ->from(Activitat::class, 'activitat')
            ->where(
                'activitat.id = :id'
            )
            ->setParameter('id', $id, IdType::TYPE_NAME)
        ;

        try {
            return $qb
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            throw ActivitatNotFound::becauseIdDoesNotExist($id);
        }
    }
}
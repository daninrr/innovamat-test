<?php

namespace App\Infrastructure\Persistence\Doctrine\Repository\Activitat\RespostaEstudiant;

use App\Domain\Model\Activitat\RespostaEstudiant;
use App\Domain\ValueObject\Id;
use Doctrine\ORM\EntityManagerInterface;

class RespostaEstudiantWriteRepository implements \App\Domain\Repository\Activitat\RespostaEstudiant\RespostaEstudiantWriteRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function save(RespostaEstudiant $respostaEstudiant): void
    {
        $this->entityManager->persist($respostaEstudiant);
        $this->entityManager->flush();
    }

    public function nextIdentity(): Id
    {
        return Id::generate();
    }
}
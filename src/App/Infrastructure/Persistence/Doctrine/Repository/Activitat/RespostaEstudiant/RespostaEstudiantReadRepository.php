<?php

namespace App\Infrastructure\Persistence\Doctrine\Repository\Activitat\RespostaEstudiant;

use App\Domain\Model\Activitat\RespostaEstudiant;
use App\Domain\ValueObject\Id;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class RespostaEstudiantReadRepository implements \App\Domain\Repository\Activitat\RespostaEstudiant\RespostaEstudiantReadRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function last(): ?RespostaEstudiant
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('resposta_estudiant')
            ->from(RespostaEstudiant::class, 'resposta_estudiant')
            ->orderBy('resposta_estudiant.creadaEn', 'DESC')
            ->setMaxResults(1)
            ->getFirstResult()
        ;

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            return null;
        }
    }

    public function previousToLast(Id $lastRespostaId): ?RespostaEstudiant
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('resposta_estudiant')
            ->from(RespostaEstudiant::class, 'resposta_estudiant')

            ->where('resposta_estudiant.id != :last_resposta_id')
            ->setParameter('last_resposta_id', $lastRespostaId)

            ->orderBy('resposta_estudiant.creadaEn', 'DESC')
            ->setMaxResults(1)
            ->getFirstResult()
        ;

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            return null;
        }
    }
}
<?php

namespace App\Infrastructure\Persistence\Doctrine\Repository\Nivell;

use App\Domain\Exception\Model\Nivell\NivellNotFound;
use App\Domain\Model\Nivell;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class NivellReadRepository implements \App\Domain\Repository\Nivell\NivellReadRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws NivellNotFound
     */
    public function ofNumeroOrFail(int $numero): Nivell
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select('nivell')
            ->from(Nivell::class, 'nivell')
            ->where(
                'nivell.numero = :numero'
            )
            ->setParameter('numero', $numero)
        ;

        try {
            return $qb
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            throw NivellNotFound::becauseNumeroDoesNotExist($numero);
        }
    }
}
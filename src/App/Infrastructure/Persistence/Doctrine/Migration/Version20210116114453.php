<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116114453 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activitat_resposta_estudiant (id CHAR(36) NOT NULL COMMENT \'(DC2Type:id)\', activitat_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:id)\', solucio VARCHAR(255) NOT NULL, temps INT NOT NULL, puntuacio INT NOT NULL, creada_en DATETIME NOT NULL, INDEX IDX_471C52AD33DBAFCE (activitat_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activitat_resposta_estudiant ADD CONSTRAINT FK_471C52AD33DBAFCE FOREIGN KEY (activitat_id) REFERENCES activitat (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE activitat_resposta_estudiant');
    }
}

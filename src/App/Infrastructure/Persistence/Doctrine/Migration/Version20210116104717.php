<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116104717 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activitat (id CHAR(36) NOT NULL COMMENT \'(DC2Type:id)\', nivell_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:id)\', nom VARCHAR(255) NOT NULL, solucio VARCHAR(255) NOT NULL, ordre INT NOT NULL, temps_total_estimat INT NOT NULL, INDEX IDX_8C630E12DEBF4025 (nivell_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nivell (id CHAR(36) NOT NULL COMMENT \'(DC2Type:id)\', numero INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activitat ADD CONSTRAINT FK_8C630E12DEBF4025 FOREIGN KEY (nivell_id) REFERENCES nivell (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activitat DROP FOREIGN KEY FK_8C630E12DEBF4025');
        $this->addSql('DROP TABLE activitat');
        $this->addSql('DROP TABLE nivell');
    }
}

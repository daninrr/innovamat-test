<?php

namespace App\Infrastructure\Api\Problem\Activitat;

use Crell\ApiProblem\ApiProblem;

class SeguentActivitatProblem extends ApiProblem
{
    public static function withException(\Exception $exception): self
    {
        return (new self('Error al obtenir la següent activitat'))
            ->setType((new \ReflectionClass($exception))->getShortName())
            ->setDetail($exception->getMessage())
            ;
    }
}
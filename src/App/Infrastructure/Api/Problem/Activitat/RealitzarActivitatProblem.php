<?php

namespace App\Infrastructure\Api\Problem\Activitat;

use Crell\ApiProblem\ApiProblem;

class RealitzarActivitatProblem extends ApiProblem
{
    public static function becauseSentDataIsErroneous(array $errors): self
    {
        $problem = (new self('Error al realitzar l\'activitat'))
            ->setDetail(
                sprintf(
                    'Els camps: %s són incorrectes',
                    implode(
                        ', ',
                        array_map(
                            function ($field) {
                                return $field;
                            },
                            array_keys(
                                $errors
                            )
                        )
                    )
                )
            )
        ;

        return $problem;
    }

    public static function withException(\Exception $exception): self
    {
        return (new self('Error al realitzar l\'activitat'))
            ->setType((new \ReflectionClass($exception))->getShortName())
            ->setDetail($exception->getMessage())
            ;
    }
}
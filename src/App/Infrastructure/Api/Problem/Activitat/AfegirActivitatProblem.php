<?php

namespace App\Infrastructure\Api\Problem\Activitat;

use Crell\ApiProblem\ApiProblem;

class AfegirActivitatProblem extends ApiProblem
{
    public static function becauseSentDataIsErroneous(array $errors): self
    {
        $problem = (new self('Error al afegir activitat'))
            ->setDetail(
                sprintf(
                    'Els camps: %s són incorrectes',
                    implode(
                        ', ',
                        array_map(
                            function ($field) {
                                return $field;
                            },
                            array_keys(
                                $errors
                            )
                        )
                    )
                )
            )
        ;

        return $problem;
    }
}